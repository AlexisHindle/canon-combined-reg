import { Component } from '@angular/core';
import { Router } from '@angular/router';
@Component({
    selector: 'app-invalid',
    templateUrl: './invalid.component.html',
    styleUrls: ['./invalid.component.scss']
})

export class InvalidComponent {

    constructor(private router: Router) { }

    finish() {
        this.router.navigate(['/']);
    }

}
