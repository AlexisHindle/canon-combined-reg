
import { Component, OnInit, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

import { environment } from 'src/environments/environment';

import Industry from '../../industry/industry.json';

import { ProfileService } from '../../services/profile.service';
import { AnalyticsService } from '../../services/analytics.service';
// import { NabBadgeService } from '../../services/nab-badge.service';
import { InfocommBadgeService } from './../../services/infocomm-badge.service';
import { AppleWalletService } from '../../services/apple-wallet.service';
import { ModalService } from './../../services/modal.service';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

    title: string = 'Complete your details';
    thanks: string = '';
    subtitle: string = 'The Canon Passport allows you experience everything the brand has to offer.';
    content: string = '';
    emailBlurred: boolean = false;
    emailConfirmedBlurred: boolean = false;
    nameBlurred: boolean = false;
    surnameBlurred: boolean = false;
    errorNAB: boolean = false;
    data: any;
    databag = {
        emailConfirm: ''
    };
    industries = Industry;
    hasBorder: boolean = false;
    isLoading: boolean = false;
    isSelected: boolean = false;
    objectId: string;
    insightLabel: string;
    invalid: boolean = false;

    constructor(public http: HttpClient, public ngZone: NgZone, public router: Router, public route: ActivatedRoute, public profileService: ProfileService, public analyticsService: AnalyticsService, public badgeService: InfocommBadgeService, public appleWalletService: AppleWalletService, public modalService: ModalService ) {}

    ngOnInit() {
        // this.isSelected = true;
        // console.log(this.isSelected, 'is selected')

        this.route.queryParams.subscribe( (params) => {
            console.log(params, 'params')
            if(params.id) {
                this.objectId = params.id;
                this.insightLabel =  params.insight_label;
                console.log(this.insightLabel, 'DONE FORM')
            }
        });

        // Setting first value and showing in dropdown.
        this.profileService.industry = "Please select one option";
        this.profileService.optin = true;

        this.route.queryParams.subscribe(params => {
            if (params) {
                console.log('params', params);
                this.data = params['qrcode'];
                console.log(this.data, 'data');
            }
        })

        this.badgeService.validate(this.data, this.profileService).then((response: any) => {
            console.log(response, 'response');
            let elements = this.data.split('|');
            console.log(this.data, 'this data');
            this.profileService.qr_code = elements[4];
            this.databag.emailConfirm = this.profileService.email; // make email confirmation match
        }, (err) => {
            console.log('heeerrrreeee');
            this.errorNAB = true;
        });
    }

    termsExcept() {
        this.profileService.t_and_c = true;
        this.modalService.close('terms-modal');
    }

    showTerms(event) {
        event.stopPropagation();
        event.preventDefault();
        this.modalService.open('terms-modal', { buttons: ['CLOSE'], title: 'Our Terms and Conditions', hasClass: 'small-modal' })
    }

    inputBlur(event) {
        if (event.target.id === 'name') {
            this.nameBlurred = true;
        }

        if (event.target.id === 'last_name') {
            this.surnameBlurred = true;
        }

        if (event.target.id === 'email') {
            this.emailBlurred = true;
        }

        // Checking if opt-in checkboxes are checked. If not add error-border class
        if (!this.profileService.optin) {
            this.hasBorder = true;
        }
    }

    changeFontColor(event) {
        this.isSelected = true;
        event.srcElement.size = 16;
        event.target.style.backgroundPosition = '97% 4%';
    }

    closeDropdown(event){
        event.srcElement.size =  0;
        event.target.style.backgroundPosition = '97% 47%';
    }

    postInteractionData(data) {
        this.http.post(environment.interactionUrl, data).subscribe((interactionResponse) => {
           let owner = data['_owner'] ? data['_owner'] : data['owner_id'];
            this.createInitialInteration(this.objectId, owner).subscribe((response) => {
                this.analyticsService.trackEvent({
                    'category': 'registration',
                    'action': 'register',
                    'qr_id': this.profileService.qr_code,
                    'label': this.objectId,
                    'industry': this.profileService.industry.toString(),
                    'insight_label': this.insightLabel,
                    'marketing_opt_in': this.profileService.optin ? "yes" : "no",
                    'type_q1': this.profileService.marketing.type_q1 ? "yes" : "no",
                    'type_q2': this.profileService.marketing.type_q2 ? "yes" : "no",
                    'type_q3': this.profileService.marketing.type_q3 ? "yes" : "no",
                    'type_q4': this.profileService.marketing.type_q4 ? "yes" : "no",
                });
            }, (error) => {
                console.log('Error: ', error);
                this.router.navigate(['/error']);
            });

            // if(this.objectId) {
            //     this.createInitialInteration(this.objectId, owner).subscribe((response) => {
            //         console.log('%c Response', 'color: purple; font-size: 20px;', response);
            //     });
            // }

        }, (error) => {
            console.log('Error:', error);
            this.router.navigate(['/error']);
        });
    }

    createInitialInteration(objectId, owner) {
        let data: any = {
            "show_id": "Canon Infocomm 2019",
            "interaction_type": "EXPERIENCE",
            "brand_id": "Canon",
            "_owner": owner,
            "payload": {
                "object_id": objectId,
                "insight_label": this.insightLabel,
                "source": "canon_combined_reg_app",
            }
        }
        return this.http.post('http://localhost:12200/api/interaction', data)
    }

    // Sends data to Xpkit
    onSubmit(valid) {
        if(this.isFormValid(valid)) {
            console.log(this.profileService.qr_code, 'profileservice qrcode');
            // Change button text and disable button
            this.isLoading = true;
            // Base data for creating an interaction

            console.log(this.profileService.data, 'profileservice data');
            let data: any = {
                "show_id": "Canon Infocomm 2019",
                "interaction_type": "REGISTRATION",
                "brand_id": "Canon",
                "_owner": this.profileService.data,
                "payload": {
                    "campaign_id": "canon-registration",
                    "industry": this.profileService.industry.toString(),
                    "object_id": this.objectId,
                    "insight_label": this.insightLabel,
                    'marketing_opt_in': this.profileService.optin ? "yes" : "no",
                    'type_q1': this.profileService.marketing.type_q1 ? "yes" : "no",
                    'type_q2': this.profileService.marketing.type_q2 ? "yes" : "no",
                    'type_q3': this.profileService.marketing.type_q3 ? "yes" : "no",
                    'type_q4': this.profileService.marketing.type_q4 ? "yes" : "no",
                    "source": "canon_combined_reg_app",
                    "notification_data": {
                        "NAME": this.profileService.first_name,
                        "QRDATA": this.profileService.qr_code
                    }
                },
                "interaction_action": "EMAIL_SEND"
            }

            this.analyticsService.trackEvent({
                'category': 'collection',
                'action': 'success',
                'label': this.objectId,
                'qr_id': this.profileService.qr_code,
                'insight_label': this.insightLabel
            });

            this.postInteractionData(data);
            let name = data._owner.first_name;
            this.router.navigate(['/thanks'], {queryParams: {'name': name}});

        } else {
            this.invalid = true;
        }
    }

    optionTicked() {
        return this.profileService.marketing.type_q1 || this.profileService.marketing.type_q2 || this.profileService.marketing.type_q3 || this.profileService.marketing.type_q4;
    }

    isFormValid(formValid) {
        // Quick Fix to industry but select logic needs to be changed
        if(formValid && this.profileService.industry !== "Please select one option" && this.optionTicked()) {
            return true;
        } else {
            return false;
        }
    }

}
