import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Product } from '../../classes/Products';
import { AnalyticsService } from '../../services/analytics.service';
import { ProfileService } from '../../services/profile.service'
import Instascan from '../../../../node_modules/imag-module-instascan-native';

import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-scan',
    templateUrl: './scan.component.html',
    styleUrls: ['./scan.component.scss']
})
export class ScanComponent implements OnInit, OnDestroy {
    scanned: boolean = false;
    scanner: any;
    objectId: string;
    insightLabel: string;

    constructor(
        private route: ActivatedRoute, 
        private router: Router, 
        private http: HttpClient, 
        private ngZone: NgZone, 
        public product: Product, 
        private analyticsService: AnalyticsService,
        public profileService: ProfileService) { }

    getIdentificationUrl() {
        return new Promise((resolve, reject) => {
            this.http.get('http://localhost:12200/config').subscribe((response: any) => {
                resolve(response.environments[response.mode].identification.url);
            });
        });
    }

    setupScanner() {
        this.scanner = new Instascan.Scanner({
            video: document.getElementById('feed'),
            continous: true,
            mirror: true
        });

        this.scanner.addListener('scan', (content) => {
            if (content) {
                this.ngZone.run(() => {
                    this.scanner.stop();
                    this.scanned = true;
                    this.scanData(content);
                });
            }
        });

        Instascan.Camera.getCameras().then((cameras) => {
            if (cameras.length > 0) {
                // grab the last item in the list which should be either the front facing camera or the IR camera
                let cameraNumber = cameras.length-1;//(cameras.length > 1) ? cameras.length-1 : 0;

                this.scanner.start(cameras[cameraNumber]);
            }
        });
    }

    IdentificationUrl() {
        return new Promise((resolve, reject) => {
            if (environment.kiosk) {
                this.http.get('http://localhost:12200/config').subscribe((response: any) => {
                    resolve(response.environments[response.mode].identification.url);
                });
            } else {
                resolve(environment.identificationUrl)
            }
        });
    }

    scanData(qrCode: string) {
        this.ngZone.run(() => {
            this.IdentificationUrl().then((response) => {
                // Removing all uncessary content from badge code - returning only necessary code to save to portal
                let element = qrCode.split('|')[1];
                this.http.get(`http://localhost:12200/api/profile?qr_code=${ element }`).subscribe((response: any) => {
                    if(response.results.length) {
                        let name = response.results.length ? response.results[0].resource.first_name : '';
                        if (this.objectId !== 'CANON_INFOCOMM_Reg') {
                            this.createInteraction(this.objectId, element).subscribe( (response: any) => {
                                this.analyticsService.trackEvent({
                                    'category': 'collection',
                                    'action': 'success',
                                    'label': this.objectId,
                                    'qr_id': element,
                                    'insight_label': this.insightLabel
                                });
                                this.router.navigate(['/thanks'], {queryParams: {'name': name}});
                                console.log('INTERACTION CREATED')

                            }, (error) => {
                                this.handleError(false);
                            });
                        } else {
                            this.router.navigate(['/thanks'], {queryParams: {'name': name}});
                        }
                        
                    } else {
                        this.router.navigate(['/register'], { queryParams: {'id': this.objectId, 'qrcode': qrCode, 'name': name, 'insight_label': this.insightLabel}, })
                    }

                    this.analyticsService.trackEvent({
                        'category': 'collection',
                        'action': 'scan',
                        'label': this.objectId,
                        'qr_id': element,
                        'insight_label': this.insightLabel
                    });

                }, (err) => {
                    console.log('Error', err);
                    if (err.status === 0) {
                        this.router.navigate(['/error']);
                    } else {
                        this.router.navigate(['/invalid']);
                    }
                });
            })
        });
    }

    createInteraction(objectId, code) {
        let data: any = {
            "show_id": "Canon Infocomm 2019",
            "interaction_type": "EXPERIENCE",
            "brand_id": "Canon",
            "payload": {
                "object_id": objectId,
                "insight_label": this.insightLabel,
                "source": "canon_combined_reg_app"
            },
            "_owner": {
                "qr_code": [code]
            }
        }
        return this.http.post('http://localhost:12200/api/interaction', data);
    }

    handleError(invalid) {
        this.analyticsService.trackEvent({
            'action': invalid ? 'non Canon QR code' : 'failed',
            'label': this.objectId
        });
        if(invalid) {
            this.router.navigate(['/register']);

        } else {
            this.router.navigate(['/error']);
        }
    }

    ngOnInit() {
        this.route.queryParams.subscribe( (params) => {
            if(params.id) {
                this.objectId = params.id;
                this.insightLabel = params.insight_label;
                // console.log(this.insightLabel, 'DONE SCAN')
                this.setupScanner();
            } else {
                this.router.navigate(['/']);
            }
        });
    }

    ngOnDestroy() {
        if(this.scanner) {
            this.scanner.stop();
        }
    }

    cancel() {
        this.router.navigate(['/']);
    }
}
