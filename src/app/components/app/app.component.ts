import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { Product } from '../../classes/Products';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
    constructor(
        private router: Router,
        private product: Product
    ) {}

    timer: any;
    inactivityTimer: any;

    ngOnInit() {
        if(!this.product.active) {
            this.router.navigate(['/settings']);
        }

        this.inactivityTimer = () => {
            window.clearTimeout(this.timer);
            this.timer = window.setTimeout( () => {
                if(this.router.url !== '/settings') {
                    this.router.navigate(['/']);
                }
            }, 30000);
        }

        window.addEventListener('click', this.inactivityTimer);
        window.addEventListener('touchstart', this.inactivityTimer);
        window.addEventListener('keydown', this.inactivityTimer);
    }

    ngOnDestroy() {
        window.removeEventListener('click', this.inactivityTimer);
        window.removeEventListener('touchstart', this.inactivityTimer);
        window.removeEventListener('keydown', this.inactivityTimer);
    }


}
