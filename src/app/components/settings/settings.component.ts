import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Product } from '../../classes/Products';

import Products from '../../products/products.json';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent {
    selected = null;
    products = Products;

  constructor(
      private router: Router,
      private product: Product
  ) {}

  select() {
      fetch('http://localhost:12000/config', {
          'method': 'post',
          'headers': {'Content-Type': 'application/json'},
          'body': JSON.stringify({'product': this.selected})
      }).then( () => {
          this.product.active = this.selected;
          this.router.navigate(['/']);
      })
  }



}
