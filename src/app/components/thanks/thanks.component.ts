import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Product } from '../../classes/Products';

@Component({
    selector: 'app-thanks',
    templateUrl: './thanks.component.html',
    styleUrls: ['./thanks.component.scss']
})

export class ThanksComponent implements OnInit, OnDestroy {
    name: string;
    timer: any;
    inactivityTimer: any;
    objectId: string;
    activeId: string;
    isCafe: boolean = false;


    constructor(
        private route: ActivatedRoute,
        private router: Router,
        public product: Product) { }

    ngOnInit() {
        this.route.queryParams.subscribe( (params) => {
            this.name =  params.name ? params.name : '';
            this.activeId = (this.product.active.id).toLowerCase();
            this.activeId == 'cafe' ? this.isCafe = true : this.isCafe = false;
        });

        window.clearTimeout(this.timer);

        this.timer = window.setTimeout( () => {
            if(this.router.url !== '/settings') {
                this.router.navigate(['/']);
            }
        }, 12500);
    }

    ngOnDestroy() {
        window.clearTimeout(this.timer);
    }

    finish() {
        this.router.navigate(['/']);
    }



}
