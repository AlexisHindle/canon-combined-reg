import { Component } from '@angular/core';
import { Router } from '@angular/router';;
import { Product } from '../../classes/Products';
import { AnalyticsService } from '../../services/analytics.service'; 
import { ModalService } from './../../services/modal.service';


@Component({
    selector: 'app-intro',
    templateUrl: './intro.component.html',
    styleUrls: ['./intro.component.scss']
})

export class IntroComponent {

    constructor(
        private router: Router,
        public product: Product,
        private analyticsService: AnalyticsService,
        public modalService: ModalService
    ) { }
    
    termsActive: boolean = false;
    link: string = this.product.active.terms_and_conditions ? this.product.active.terms_and_conditions : '';

    nextPage(name, valid) {
        if(valid) {
            this.analyticsService.trackEvent({
                'action': 'start',
                'label': name,
                'insight_label': this.product.active.id
            });
        }

        if(this.product.active.terms_and_conditions) {
            this.modalService.open('terms-modal', { title: 'CarePAK PLUS Sweepstakes at NAB 2019 Official Rules', hasClass: 'small-modal' });
        } else {
            if(valid) {
                this.router.navigate(['scan'], {queryParams: {'id': name, 'insight_label': this.product.active.id }});
            }
        }
    }

    openTerms(event) {
        this.modalService.open('terms-modal', { title: 'CarePAK PLUS Sweepstakes at NAB 2019 Official Rules', hasClass: 'small-modal' })
        event.stopPropagation();
    }

    termsExcept(name, valid) {
        if(valid) {
                this.router.navigate(['scan'], {queryParams: {'id': name, 'insight_label': this.product.active.id }});
        }
    }
}
