import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { Subject } from 'rxjs';

import { ModalService, ModalButtonType, ModalOpenArgs } from './../../services/modal.service';

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

    @Input() id: string;
    private element: any;
    status = false;
    buttonClicked: Subject<ModalButtonType> = null;

    options: ModalOpenArgs = {
        title: 'TITLE',
        buttons: ['OK'],
        hasClass: ''
    };

    constructor(
        private modalService: ModalService,
        private el: ElementRef,
    ) {
        this.element = el.nativeElement;
    }

    ngOnInit(): void {
        const modal = this;

        // Ensure id attribute exists
        if (!this.id) {
            console.error('Modal must have an id');
            return;
        }

        // Move element to bottom of the page (just before </body>) so it can be displayed above everything
        document.body.appendChild(this.element);

        // Add self (this modal instance) to the modal service so it's accessible from controllers
        this.modalService.add(this);
    }

    // Remove self from modal service when directive is destroyed
    ngOnDestroy(): void {
        this.modalService.remove(this.id);
        this.element.remove();
    }

    action(button: ModalButtonType) {
        this.buttonClicked.next(button);
        this.buttonClicked.complete();
        this.element.style.display = 'none';
        document.body.classList.remove('modal-open');
    }

    open(options: ModalOpenArgs) {
        this.buttonClicked = new Subject<ModalButtonType>();

        this.options = Object.assign({
            title: 'No title',
            buttons: ['CLOSE'],
        }, options);

        this.element.style.display = 'block';
        document.body.classList.add('modal-open');

        return this.buttonClicked;
    }


}
