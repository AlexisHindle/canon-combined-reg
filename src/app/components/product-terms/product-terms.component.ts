import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Product } from '../../classes/Products';


@Component({
    selector: 'product-terms',
    templateUrl: './product-terms.component.html',
    styleUrls: ['./product-terms.component.scss']
})
export class ProductTermsComponent implements OnInit {
    @Input() link: any;
    @Output() close = new EventEmitter<boolean>();
    text: any;

    constructor(private http: HttpClient) {}

    ngOnInit() {
        this.http.get(this.link, {'responseType': 'text'}).subscribe( response => {
            this.text = response;
        });
    }

    closeOverlay() {
        this.close.emit();
    }
}
