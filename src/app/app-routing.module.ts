import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsComponent } from './components/settings/settings.component';
import { IntroComponent } from './components/intro/intro.component';
import { ScanComponent } from './components/scan/scan.component';
import { InvalidComponent } from './components/invalid/invalid.component';
import { FormComponent } from './components/form/form.component';
import { ErrorComponent } from './components/error/error.component';
import { ThanksComponent } from './components/thanks/thanks.component';

const routes: Routes = [
    {
        path: 'settings',
        component: SettingsComponent
    },
    {
        path: '',
        component: IntroComponent
    },
    {
        path: 'register',
        component: FormComponent
    },
    {
        path: 'scan',
        component: ScanComponent
    },
    {
        path: 'thanks',
        component: ThanksComponent
    },
    {
        path: 'error',
        component: ErrorComponent
    },
    {
        path: 'invalid',
        component: InvalidComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            routes,
            {
                enableTracing: false,
                useHash: true
            }
        )
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
