import { Injectable, ElementRef } from '@angular/core';
import { Subject, Observable } from 'rxjs';

export interface KeyboardStateEventInfo {
    state: boolean;
}

@Injectable({
    providedIn: 'root'
})
export class KeyboardService {

    private _shift = false; // denotes whether the shift key is pressed
    private _alpha = true; // denotes whether the alpha keyboard is displaying (otherwise the numeric keyboard is showing)
    private _activeElement: ElementRef = null; // the currently active input element that has focus
    private _maxLength = 0; // the currently elements maxLength attribute value

    alphaChangeEvent: Subject<KeyboardStateEventInfo> = new Subject(); // triggered when the alpha state changes
    shiftChangeEvent: Subject<KeyboardStateEventInfo> = new Subject(); // triggered when the shift stage changes

    constructor() { }

    get isTouchDevice() {
        return true;
        //return ('ontouchstart' in document.documentElement);
    }

    set shift(flag: boolean) {
        if (this._shift !== flag) {
            this._shift = flag;
            this.shiftChangeEvent.next({ state: flag });
        }
    }

    get shift(): boolean {
        return this._shift;
    }

    set alpha(flag: boolean) {
        if (this._alpha !== flag) {
            this._alpha = flag;
            this.alphaChangeEvent.next({ state: flag });
        }
    }

    get alpha(): boolean {
        return this._alpha;
    }

    set activeElement(element) {
        this._activeElement = element;
        this._maxLength = parseInt(this._activeElement.nativeElement.getAttribute('maxLength', 0));
    }

    get activeElement() {
        return this._activeElement;
    }

    get maxLength() {
        return (this._maxLength > 0) ? this._maxLength : 9999;
    }

}
