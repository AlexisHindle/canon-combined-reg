import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KeyboardComponent } from './components/keyboard/keyboard.component';
import { KeyboardKeyComponent } from './components/key/key.component';
import { KeyboardDirective } from './directives/keyboard.directive';

@NgModule({
    declarations: [
        KeyboardComponent,
        KeyboardKeyComponent,
        KeyboardDirective
    ],
    imports: [
        CommonModule
    ],
    exports: [
        KeyboardComponent,
        KeyboardKeyComponent,
        KeyboardDirective
    ]
})
export class KeyboardModule { }
