import { Directive, HostListener, ElementRef } from '@angular/core';
import { KeyboardService } from '../services/keyboard.service';

@Directive({
    selector: '[appKeyboard]'
})
export class KeyboardDirective {

    @HostListener('focus') onFocus() {
        this.keyboardService.activeElement = this.elementRef;
    }

    @HostListener('blue') onBlur() {
        this.keyboardService.activeElement = null;
    }

    constructor(
        private keyboardService: KeyboardService,
        private elementRef: ElementRef
    ) { }

}
