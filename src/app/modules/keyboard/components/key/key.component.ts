import { Component, Input, ViewChild, TemplateRef, ElementRef } from '@angular/core';
import { KeyboardService } from '../../services/keyboard.service';

@Component({
    selector: 'app-keyboard-key',
    templateUrl: './key.component.html',
    styleUrls: ['./key.component.scss']
})
export class KeyboardKeyComponent {

    @Input('data-key') key: string;
    @Input('template-id') _templateId: string;

    @ViewChild('KEY_DEL') KEY_DEL: TemplateRef<any>;
    @ViewChild('KEY_SHIFT') KEY_SHIFT: TemplateRef<any>;
    @ViewChild('KEY_LEFT') KEY_LEFT: TemplateRef<any>;
    @ViewChild('KEY_RIGHT') KEY_RIGHT: TemplateRef<any>;
    @ViewChild('ALPHA') ALPHA: TemplateRef<any>;
    @ViewChild('NUMERIC') NUMERIC: TemplateRef<any>;

    private _press: any;

    constructor(
        private keyboardService: KeyboardService,
        private elementRef: ElementRef
    ) {
        this._press = this.press.bind(this);
        this.elementRef.nativeElement.addEventListener(this.keyboardService.isTouchDevice ? 'touchstart' : 'mousedown', this._press);
    }

    get templateId(): string {
        return this._templateId ? this._templateId.toUpperCase() : '';
    }

    get caption(): string {
        if (this.templateId.length > 0) {
            return '';
        } else {
            return this.keyboardService.shift ? this.key.toUpperCase() : this.key;
        }
    }

    get templateRef() {
        return (this.templateId.length > 0) ? this[this.templateId] : null;
    }

    press(evt) {

        this.elementRef.nativeElement.classList.remove('flash');

        setTimeout(() => this.elementRef.nativeElement.classList.add('flash'));

        const target: ElementRef = this.keyboardService.activeElement;

        // These keys are not dependent on there being an active input
        switch (this.key) {
            case '[SHIFT]':
                this.keyboardService.shift = !this.keyboardService.shift;
                break;

            case '[ALPHA]':
                this.keyboardService.alpha = true;
                break;

            case '[NUMERIC]':
                this.keyboardService.alpha = false;
                break;
        }

        if (target === null) {
            return; // no input has focus
        }

        let validTarget = target && target.nativeElement.tagName === 'INPUT';
        let value = validTarget ? target.nativeElement.value : '';
        let newValue = value;
        let cursorPos = validTarget ? target.nativeElement.selectionStart : 0;

        switch (this.key) {

            case '[DEL]':
                newValue = value.slice(0, target.nativeElement.selectionStart - 1) + value.slice(target.nativeElement.selectionEnd);
                cursorPos--;
                break;

            case '[LEFT]':
                cursorPos--;
                break;

            case '[RIGHT]':
                cursorPos++
                break;

            default:
                newValue = value.slice(0, target.nativeElement.selectionStart) + this.caption + value.slice(target.nativeElement.selectionEnd);
                this.keyboardService.shift = false; // turn off shift key
                cursorPos++;
                break;
        }

        if (cursorPos < 0) {
            cursorPos = 0;
        }

        if (validTarget) {

            // make sure length is not exceeded
            let maxLength = this.keyboardService.maxLength;
            if (maxLength > 0 && newValue.length > maxLength) {
                evt.preventDefault();
                return;
            }

            target.nativeElement.value = newValue;
            target.nativeElement.setSelectionRange(cursorPos, cursorPos);

            // the following will make sure the current cursor position is visible
            target.nativeElement.blur();
            target.nativeElement.focus();
            target.nativeElement.dispatchEvent(new Event('change'));
        }

        evt.preventDefault();
    }

}
