import { Component, OnInit, OnDestroy, Output, EventEmitter, HostBinding } from '@angular/core';
import { KeyboardService, KeyboardStateEventInfo } from '../../services/keyboard.service';
import { Observable, Subscription } from 'rxjs';

@Component({
    selector: 'app-keyboard',
    templateUrl: './keyboard.component.html',
    styleUrls: ['./keyboard.component.scss']
})
export class KeyboardComponent implements OnInit, OnDestroy {

    // Add class to :host component based on whether showing alpha or numeric
    @HostBinding('class.alpha') alpha: boolean = true;

    private _alphaChangeListener: Subscription = null;

    constructor(private keyboardService: KeyboardService) {

        // Listen for changes to the alpha key state changes
        this._alphaChangeListener = this.keyboardService.alphaChangeEvent.subscribe((info: KeyboardStateEventInfo) => {
            this.alpha = info.state;
        });

    }

    ngOnInit() {
    }

    ngOnDestroy() {

        // cleanup listeners
        if (this._alphaChangeListener) {
            this._alphaChangeListener.unsubscribe();
        }

    }


}
