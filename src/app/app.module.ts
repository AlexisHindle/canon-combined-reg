import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { Product } from './classes/Products';

import { ApploadService } from './services/appload.service';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './components/app/app.component';
import { IntroComponent } from './components/intro/intro.component';
import { ScanComponent } from './components/scan/scan.component';
import { ThanksComponent } from './components/thanks/thanks.component';
import { SettingsComponent } from './components/settings/settings.component';
import { ErrorComponent } from './components/error/error.component';
import { InvalidComponent } from './components/invalid/invalid.component';
import { FormComponent } from './components/form/form.component';
import { QrScanComponent } from './modules/qr-scan/qr-scan.component';
import { ModalComponent } from './components/modal/modal.component';
import { ProductTermsComponent } from './components/product-terms/product-terms.component';

export function loadApp(appLoad: ApploadService) {
    return () => appLoad.initializeApp();
}

@NgModule({
    declarations: [
        AppComponent,
        IntroComponent,
        ScanComponent,
        ThanksComponent,
        SettingsComponent,
        ErrorComponent,
        InvalidComponent,
        FormComponent,
        QrScanComponent,
        ModalComponent,
        ProductTermsComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule
    ],
    providers: [
        Product,
        ApploadService,
        { provide: APP_INITIALIZER, useFactory: loadApp, deps: [ApploadService], multi: true}
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
