import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from './../classes/Products';

@Injectable()
export class ApploadService {
    constructor(
        private httpClient: HttpClient,
        private product: Product
  ) {}

    setEnvironment(): Promise<any> {
        return new Promise((resolve, reject) => {
            fetch('http://localhost:12000/app/package.json')
                .then( (response) => response.json() )
                .then( (response) => {
                    fetch('http://localhost:12200/config', {
                        'method': 'put',
                        'headers': {
                            'Content-Type': 'application/json'
                        },
                        'body': JSON.stringify({
                            environments: response.environments
                        })
                    }).then( () => {
                        console.log('%c resonse', 'color: red; font-size: 18px;', response );
                        resolve();
                    })
                });
        });
    }

    getSettings(): Promise<any> {
        return new Promise((resolve, reject) => {
            fetch('http://localhost:12000/config')
                .then( (response) => response.json())
                .then( (response) => {
                    if(response.product) {
                        this.product.active = response.product
                    }
                    resolve();
                })
        });
    }

    initializeApp(): Promise<any> {
        return new Promise((resolve, reject) => {
            Promise.all([this.setEnvironment(), this.getSettings()]).then( () => {
                resolve();
            });
        });
    }
}
