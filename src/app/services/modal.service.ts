import { element } from '@angular/core/src/render3';
import { Injectable } from '@angular/core';

export type ModalButtonType = 'OK' | 'CLOSE'

// Might be overkill
export interface ModalOpenArgs {
    buttons?: Array<ModalButtonType>;
    title?: string;
    hasClass: string;
}

@Injectable({
    providedIn: 'root'
})
export class ModalService {

    private modals: any[] = [];

    add(modal: any) {
        // add modal to array of active modals
        this.modals.push(modal);
    }

    remove(id: string) {
        // remove modal from array of active modals
        this.modals = this.modals.filter(x => x.id !== id);
    }

    open(id: string, options: ModalOpenArgs) {
        // open modal specified by id
        const modal: any = this.modals.filter(x => x.id === id)[0];
        return modal.open(options);
    }

    close(id: string) {
        // close modal specified by id
        const modal: any = this.modals.filter(x => x.id === id)[0];
        modal.element.style.display = 'none';
        // modal.close();
    }
}
