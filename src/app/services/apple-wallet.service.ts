import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class AppleWalletService {

    constructor(
        public http: HttpClient
    ) { }

    create(code) {
        return new Promise((resolve, reject) => {
            let data = {
                'configuration_id': 'canon_apple_wallet',
                'data': {
                    'unique_code': code
                }
            }

            // As this is a kiosk app no check needed like in other app
            this.http.get('http://localhost:12200/api/o/token').subscribe((response: any) => {
                let token = response.access_token;

                this.http.get('http://localhost:12200/config').subscribe((config: any) => {
                    let url = config.environments[config.mode].identification.url;
                    let headers = {
                        headers: new HttpHeaders({
                            'Authorization': 'Bearer ' + token
                        })
                    }

                    this.http.post(`${url}/api/identity/apple/wallet`,data, headers).subscribe((wallet: any) => {
                        resolve(wallet.resource_url);
                    }, (error) => {
                        console.log('error', error);
                        reject();
                    });
                });
            }, () => {
                console.log('error: could not get a token');
            });
        })
    }
}
