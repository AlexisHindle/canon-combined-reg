import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { ProfileService } from './profile.service';


    @Injectable({
        providedIn: 'root'
    })
export class InfocommBadgeService {
    currentCall: any;

    // This defines how the fields from the Infocomm API will map to the profile
    fieldMappings: any = {
        "Address": "address.line1",
        "Address 2": "address.line2",
        "Badge Number": "infocomm_badge_number",
        "City": "address.city",
        "Company": "infocomm_company",
        "Company 2": "infocomm_company_2",
        "Country": "address.country_name",
        "Country Code": "address.country",
        "Email Address": "email",
        "Fax Area Code": "infocomm_fax_area_code",
        "Fax Number": "infocomm_fax_number",
        "First Name": "first_name",
        "First Name Prefix": "infocomm_first_name_prefix",
        "Last Name": "last_name",
        "Last Name Suffix": "infocomm_last_name_suffix",
        "Mail Code": "infocomm_mail_code",
        "Middle Initial": "infocomm_middle_initial",
        "Phone Area Code": "infocomm_phone_area_code",
        "Phone Number": "phone_number",
        "State": "address.district",
        "Title": "infocomm_title"
    }
    constructor(
        private http: HttpClient
    ) { }

    // Updates the passed profile from the Infocomm API data, any fields that cannot be directly mapped to a standard
    // profile field will be prefixes with infocomm
    generateProfileFromInfocommData(data: any, profile: ProfileService):any {
        Object.keys(data).forEach(infocommFN => {
            const mapping = this.fieldMappings[infocommFN];
            if (mapping) {
                const profileFN = this.fieldMappings[infocommFN];

                // address fields and email are handled differently
                if (profileFN.startsWith('address')) {
                    profile.address[profileFN.substr(8)] = data[infocommFN];
                } else if (profileFN === 'email') {
                    profile.email = data[infocommFN];
                } else {
                    profile.setValue(profileFN, data[infocommFN]);
                }
            }
        });

        return profile;
    }

    // Check the passed qr code and if valid will attempt to retrieve the data from the Infocomm API
    validate(badgeData: string, profile: ProfileService) {
        return new Promise((resolve, reject) => {

            // Extract badge id from data
            const data: string = (badgeData || '').trim();
            if (data.length === 0) {
                return reject(new Error('badge data is 0 length!'));
            }

            // Make sure its a valid badge
            const elements: Array<string> = data.split('|');
            const badgeId: string = (elements.length > 1) ? elements[1].trim() : '';
            if (badgeId.length === 0) {
                return reject(new Error('badge id is 0 length!'));
            }

            // Get the first letter of the last name
            const check: string = (elements.length > 3) ? (elements[3].trim() + '').substr(0, 1) : '';

            // General response error handler
            const errorHandler = (e) => {
                console.warn('Failed to call badge api!', e.toString());
                if (elements.length > 3) {
                    console.info('Falling back to Badge Data');
                    resolve(this.generateProfileFromInfocommData({
                        "First Name": elements[2],
                        "Last Name": elements[3]
                    }, profile));
                } else {
                    reject(e);
                }
            }

            // Call the proxy service
            this.currentCall = this.http.get(`${ environment.NABAPIUrl }?badge_id=${ badgeId }&event_id=${ environment.NABAPIEventId }&check=${ check }`).subscribe((data: any) => {
                if (data['error_code']) {
                    errorHandler(new Error(`[${data['error_code']}] ${data['error_message']}`));
                } else {
                    resolve(this.generateProfileFromInfocommData(data, profile));
                }
            }, (e) => {
                console.log('error on http', e);
                reject();
            }); 
        });
    }

    cancelAll() {
        if (this.currentCall) {
            this.currentCall.unsubscribe();
        }
    }
}
