import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ProfileService {

    private _data: any = {};

    constructor() { }

    reset() {
        this._data = {};
    }

    debug() {
        console.log(this._data);
    }

    set data(data: any) {
        // clone the data into our instance data ( into string and then back into json...decouples references)
        this._data = JSON.parse(JSON.stringify(data));
    }

    get data(): any {
        return this._data;
    }

    setValue(name: string, value: any) {
        this._data[name] = value;
    }

    getValue(name: string): any {
        return this._data[name];
    }

    get first_name(): string {
        return this.data['first_name'] || '';
    }

    set first_name(value: string) {
        this.data['first_name'] = value;
    }

    get last_name(): string {
        return this.data['last_name'] || '';
    }

    set last_name(value: string) {
        this.data['last_name'] = value;
    }

    // email and qr_code are actually arrays in a profile, but as we are only dealing with a single one here
    // they were make the getters/settings strings and then massage into arrays

    get email(): string {
        const emails = this.data['email'] || [];
        return emails.length > 0 ? emails[0] : '';
    }

    set email(value: string) {
        this.data['email'] = [value];
    }

    get industry(): string {
        return this.data['industry'].toString();
    }

    set industry(value: string) {
        this.data['industry'] = [value];
    }

    get qr_code(): string {
        const qr_codes = this.data['qr_code'] || [];
        return qr_codes.length > 0 ? qr_codes[0] : '';
    }

    set qr_code(value: string) {
        this.data['qr_code'] = [value];
    }

    addQrCode(code: string) {
        this.data['qr_code'] = this.data['qr_code'] || []; // make sure we have an array to work with

        // only add to current array if not already exists
        if (this.data['qr_code'].indexOf(code) < 0) {
            this.data['qr_code'].push(code);
        }

    }

    get address(): any {
        // Make sure there is a place for address data to go
        if (typeof this.data['address'] === 'undefined') {
            this._data['address'] = [];
        }

        if (this._data['address'].length === 0) {
            this._data['address'][0] = {};
        }

        return this._data['address'][0];
    }

    get marketing(): any {
        if (typeof this.data['marketing'] === 'undefined') {
            this._data['marketing'] = {}
        }

        return this._data['marketing'];
    }

    get optin(): boolean {
        return Boolean(this._data['optin']);
    }

    set optin(value: boolean) {
        this._data['optin'] = value;
    }

    get t_and_c(): boolean {
        return (this._data['t_and_c'] || '') === 'AGREE';
    }

    set t_and_c(value: boolean) {
        this._data['t_and_c'] = value ? 'AGREE' : '';
    }

    get apple_wallet(): string {
        return this._data['apple_wallet'];
    }

    set apple_wallet(value: string) {
        this._data['apple_wallet'] = value;
    }

}
