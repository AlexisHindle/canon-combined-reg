import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { ProfileService } from './profile.service';


@Injectable({
    providedIn: 'root'
})
export class NabBadgeService {
    currentCall: any;

    fieldMappings: any = {
        "LeadID": "nab_lead_id",
        "CapturedDate": "nab_captured_date",
        "CapturedBy": "nab_captured_by",
        "ConnectKey": "nab_connect_key",
        "FirstName": "first_name",
        "LastName": "last_name",
        "Title": "nab_title",
        "Company": "nab_company",
        "Company2": "nab_company2",
        "Address": "address.line1",
        "Address2": "address.line2",
        "Address3": "nab_address_3",
        "City": "address.city",
        "StateCode": "address.state",
        "ZipCode": "address.code",
        "CountryCode": "address.country",
        "Email": "email",
        "Phone": "phone_number",
        "PhoneExtension": "nab_phone_extension",
        "Fax": "nab_fax",
        "Notes": "nab_notes"
    }

    constructor(private http: HttpClient) { }

    generateProfileFromNABData(data: any, profile: ProfileService): any {
        Object.keys(data.LeadInfo).forEach(nabFN => {
            const mapping = this.fieldMappings[nabFN];
            if (mapping) {
                const profileFN = this.fieldMappings[nabFN];
            
                // address fields and emails are handled differently
                if(profileFN.startsWith('address')) {
                    profile.address[profileFN.substr(8)] = data.LeadInfo[nabFN];
                } else if (profileFN === 'email') {
                    profile.email = data.LeadInfo[nabFN];
                } else {
                    profile.setValue(profileFN, data.LeadInfo[nabFN]);
                }

            }
        });

        if(data.Demographics) {
            data.Demographics.forEach(demographic => {
                profile.setValue(`nab_${demographic.Key}`, demographic.Value);
            })
        }

        return profile;
    }

    // Checks the passed qr code and if valid will attempt to retrieve the data from NAB api
    validate(badgeData: string, profile: ProfileService) {
        return new Promise((resolve, reject) => {
            // Extract badge Id from data
            const data: string = (badgeData || '').trim();

            if(data.length === 0) {
                return reject(new Error('badge data is 0 length!'));
            }

            // Make sure its a valid badge
            if(!data.includes('HTTPS://L4E.US')) {
                return reject(new Error('invalid L4E badge'));
            }

            // General response error handler
            const errorHandler = (e) => {
                console.log(`Failed to call badge api`, e.toString());
                let elements = data.split('/');
                if(elements.length > 6) {
                    console.info(`Falling back to Badge Data`);
                    resolve(this.generateProfileFromNABData({
                        "LeadInfo": {
                            "FirstName": elements[5],
                            "LastName": elements[6]
                        }
                    }, profile));
                } else {
                    reject(e);
                }
            }

            let elements = data.split('/');
            this.currentCall = this.http.get(`${environment.NABAPIUrl}?badgeid=97757&barcode=${encodeURIComponent(badgeData)}`).subscribe((data: any) => {
                if (!data['Success']) {
                    errorHandler(new Error(`[${data['Messages'][0].MessageID}] ${data['Messages'][0].Message}`));
                } else {
                    resolve(this.generateProfileFromNABData(data, profile));
                }
            }, (e) => {
                console.log('error on http', e);
                reject();
            });
        });
    }

    cancelCall() {
        if (this.currentCall) {
            this.currentCall.unsubscribe();
        }
    }


}
