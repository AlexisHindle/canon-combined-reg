import { Injectable } from '@angular/core';
import { Analytics } from 'imag-xpkit-analytics-library';

@Injectable({
    providedIn: 'root'
})
export class AnalyticsService {

    constructor() { }

    analytics = new Analytics('wf7vKrjum0dU9RvhTg446gmJXJI5dZV3', true);
    // analytics = new Analytics('6Rv9GMZgE5vCpKPUMFBOuVgx6qZ4z6N8', true);

    consistentData = {
        "app_name": "Canon Combined Registration Product App",
        "app_instance_id": "canon-combined-reg-product-app",
        "category": "collection",
        "brand_name": "Canon",
        "show_name": "Canon Infocomm 2019",
        "country": "USA",
        "city": "Orlando",
        "language": "en_US",
        "touchpoint": "play"
    }

    trackEvent(data) {
        this.analytics.track("event", Object.assign({}, this.consistentData, data));
    }

    trackPage(pageName, data) {
        this.analytics.page(pageName, Object.assign({}, this.consistentData, data));
    }
};
