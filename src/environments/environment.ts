import { AppEnv }  from './appenv.type';


export const environment: AppEnv = {
    production: false,
    kiosk: true,
    interactionUrl: 'http://localhost:12200/api/interaction',
    identificationUrl: '',
    profileUrl: '',
    appleWalletUrl: '/api/wallet',
    NABAPIEventId: 208,
    NABAPIUrl: 'https://badge.canon-na.xp.imagination.net/api/lead-api/'

}