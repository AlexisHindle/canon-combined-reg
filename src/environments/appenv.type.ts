export interface AppEnv {
    production: boolean,
    kiosk: boolean,
    identificationUrl: string,
    interactionUrl: string,
    profileUrl: string,
    NABAPIEventId: number,
    NABAPIUrl: string,
    appleWalletUrl: string
}
